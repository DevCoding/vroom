import { ref, firebaseAuth,firebaseStorage } from '../config/constant'

export function auth (email, pw, value) {
  return firebaseAuth().createUserWithEmailAndPassword(email, pw)

}

export function logout () {
  return firebaseAuth().signOut()
}

export function login (email, pw) {
  return firebaseAuth().signInWithEmailAndPassword(email, pw)
}

export function resetPassword (email) {
  return firebaseAuth().sendPasswordResetEmail(email)
}
export function saveVrcontent (value) {

    var vrcontentdb = ref.child("VRContent");

    let imageFileName = value.title;
    if (value.imageFile.name.toLowerCase().includes('jpg') == true){
        imageFileName = imageFileName.concat('.jpg');
    } 
    if (value.imageFile.name.toLowerCase().includes('jpeg') == true){
        imageFileName = imageFileName.concat('.jpeg');
    } 
    if (value.imageFile.name.toLowerCase().includes('png') == true){
        imageFileName = imageFileName.concat('.png');
    } 
    if (value.imageFile.name.toLowerCase().includes('bmp') == true){
        imageFileName = imageFileName.concat('.bmp');
    } 
    var upload = {
        title : value.title, 
        description : value.description, 
        ratings : value.ratings,
        image : ""
    }
    firebaseStorage.child('vrcontent/' + imageFileName).put(value.imageFile).then(function(snap){
        console.log(value)

        upload["image"] = snap.metadata.downloadURLs[0]
        console.log(upload)

        vrcontentdb.push(upload).then(() => {
            alert("Successfully registered")
        });
    });


}
export function saveMusic (value){

    var musicdb = ref.child("Music")
    let imageFileName = value.audiotitle;
    if (value.imageFile.name.toLowerCase().includes('jpg') == true){
        imageFileName = imageFileName.concat('.jpg');
    } 
    if (value.imageFile.name.toLowerCase().includes('jpeg') == true){
        imageFileName = imageFileName.concat('.jpeg');
    } 
    if (value.imageFile.name.toLowerCase().includes('png') == true){
        imageFileName = imageFileName.concat('.png');
    } 
    if (value.imageFile.name.toLowerCase().includes('bmp') == true){
        imageFileName = imageFileName.concat('.bmp');
    } 
    let audioFileName = value.audiotitle;

    let ext = value.audioFile.name.substring(value.audioFile.name.indexOf("."));

    audioFileName = audioFileName.concat(ext);
    

    firebaseStorage.child('music/artwork/' + imageFileName).put(value.imageFile).then(function(snap1){
        let artworkURL = snap1.metadata.downloadURLs[0];
        firebaseStorage.child('music/audio/' + audioFileName).put(value.audioFile).then(function(snap2){
            let audioURL = snap2.metadata.downloadURLs[0];
            let upload = {
                artist : value.artist,
                audiotitle : value.audiotitle,
                genre : value.genre,
                description : value.description,
                audio : audioURL,
                artwork : artworkURL
            }
            console.log(upload)
            musicdb.push(upload).then(() => {
                alert("Successfully registered")
            });
            
        });    
    });
}
export function saveUser (value, type) {
    //storage.child(fname + lname + )
    if (type == "driver"){
        let fileName = value.fname.concat(value.lname);
        if (value.imageFile.name.toLowerCase().includes('jpg') == true){
            fileName = fileName.concat('.jpg');
        } 
        if (value.imageFile.name.toLowerCase().includes('jpeg') == true){
            fileName = fileName.concat('.jpeg');
        } 
        if (value.imageFile.name.toLowerCase().includes('png') == true){
            fileName = fileName.concat('.png');
        } 
        if (value.imageFile.name.toLowerCase().includes('bmp') == true){
            fileName = fileName.concat('.bmp');
        } 
        let driverdb = ref.child("Driver");
    
        
    
        firebaseStorage.child('driver/' + fileName).put(value.imageFile).then(function(snap){
    
            let upload = {
                fname : value.fname, 
                lname : value.lname, 
                title : value.title, 
                majordept : value.majordept,
                email : value.email,
                phone : value.phone,
                fquote : value.fquote,
                photo : snap.metadata.downloadURLs[0]
            }
    
            driverdb.push(upload).then(() => {
                alert("Successfully signed up")
            });
        });
    }
    else {
        let passengerdb = ref.child("Passenger");

        passengerdb.push(value).then(() => {
            alert("Successfully signed up")
        });
    }
    
}
