/**
 * Main store function
 */
import { createStore, applyMiddleware, compose } from 'redux'

import createHistory from 'history/createBrowserHistory'

import { routerMiddleware } from 'react-router-redux'
import reducers from './reducer';

const history = createHistory()
export function configureStore(initialState = {}) {
    const middleware = routerMiddleware(history)
    const store = createStore(
      reducers,
      applyMiddleware(middleware)
    )
    
    return { store, history }
}
