import Types from './types'

export const signupRequested = (info) =>
({ type: Types.SIGNUP_REQUESTED, info })

export const signupSucceeded = (info) =>
({ type: Types.SIGNUP_SUCCEEDED, info })

export const signupFailed = (err) =>
({ type: Types.SIGNUP_FAILED, err: err })


export const loginRequested = (email, password) =>
({ type: Types.LOGIN_REQUESTED, email, password})

export const loginSucceeded = (info) =>
({ type: Types.LOGIN_SUCCEEDED, info })

export const loginFailed = (err) =>
({ type: Types.LOGIN_FAILED, err: err })