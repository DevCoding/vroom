import { createTypes } from 'reduxsauce'

export default createTypes(`
    LOGIN_REQUESTED
    LOGIN_SUCCEEDED
    LOGIN_FAILED

    SIGNUP_REQUESTED
    SIGNUP_SUCCEEDED
    SIGNUP_FAILED
`)