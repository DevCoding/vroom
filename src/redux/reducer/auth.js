import { createReducer } from 'reduxsauce'
import Types from '../action/types'
const initialState = {
    state : "NOT_LOGGED",
}

// export default function appReducer(state = initialState, action) {
//     switch(action.type) {
//         case "LOGIN_REQUESTED" : 
//             console.log(action)
//             return {...state,  email : action.email, password : action.password}
//         default : return state;
//     }
// }



export const signupRequest = (state = initialState, action) => {
    return { state: 'SIGNING_UP' }
}
export const signupSucceeded = (state = initialState, action) => {
    return { state: 'SIGNUP_SUCCEEDED' }
}
export const signupFailed = (state = initialState, action) => {
    return { state: 'SIGNUP_FAILED' }
}
export const loginRequested = (state = initialState, action) => {
    console.log(state)
    console.log(action)
    return { state: 'LOGGING_IN' }
}
export const loginSucceeded = (state = initialState, action) => {
    return { state: 'LOGIN_SUCCEEDED' }
}
export const loginFailed = (state = initialState, action) => {
    return { state: 'LOGIN_FAILED' }
}

export const handlers = {
    [Types.LOGIN_REQUESTED] : loginRequested,
    [Types.LOGIN_SUCCEEDED] : loginSucceeded,
    [Types.LOGIN_FAILED] : loginFailed,
    [Types.SIGNUP_REQUESTED] : signupRequest,
    [Types.SIGNUP_SUCCEEDED] : signupSucceeded,
    [Types.SIGNUP_FAILED] : signupFailed,
}
export default createReducer(initialState, handlers)
