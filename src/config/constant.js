import firebase from 'firebase'

const config = {
    apiKey: "AIzaSyCBGYcI1q9lVVFP-04wWDPPizvnZGP_ipc",
    authDomain: "vrproject-6e02a.firebaseapp.com",
    databaseURL: "https://vrproject-6e02a.firebaseio.com",
    projectId: "vrproject-6e02a",
    storageBucket: "vrproject-6e02a.appspot.com",
    messagingSenderId: "47883862438"
}

firebase.initializeApp(config)

export const ref = firebase.database().ref()
export const firebaseAuth = firebase.auth
export const firebaseStorage = firebase.storage().ref()