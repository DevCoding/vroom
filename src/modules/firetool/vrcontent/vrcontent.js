import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import CircularProgress from 'material-ui/CircularProgress';
import CustomTextField from '../../../components/customTextfield/customTextfield'

import { saveVrcontent } from '../../../helpers/auth'
class Vrcontent extends Component {
  constructor(props){
    super(props)
    
    this.state = {
        title : '', description : '', ratings : '',            
        image : null,
        imageFile : null,
    };
    this.handleClick = this.handleClick.bind(this)
    this.onChange = this.onChange.bind(this)
    this.onAvatar = this.onAvatar.bind(this);
    this.onFilePick = this.onFilePick.bind(this);
  }
  handleClick = (e) => {

      e.preventDefault();

      console.log(this.state)
      if (this.state.title && this.state.description && this.state.ratings && this.state.image && this.state.imageFile){
        saveVrcontent(this.state)

        var state = this.state
        state["title"] = ""
        state["description"] = ""
        state["ratings"] = ""
        state["image"] = null
        state["imageFile"] = null
        this.setState(state)

        this.titleInput.setState({text : ""})
        this.descriptionInput.setState({text : ""})
        this.ratingInput.setState({text : ""})
      } else{
        alert("Validation Failed")
      }
      
  };
  onChange(value, type){
    if (type == "vrtitle"){
        var state = this.state
        state["title"] = value.text
        this.setState(state)
    }
        
    if (type == "vrdescription"){
        var state = this.state
        state["description"] = value.text
        this.setState(state)
    }
    if (type == "vrrating"){
        var state = this.state
        state["ratings"] = value.text
        this.setState(state)
    }
}
  onAvatar(e) {
    this.filePicker.click()
  }
  onFilePick(e) {
    e.preventDefault()

    const file = e.target.files[0]

    if (file) {
        let reader = new FileReader()

        reader.onloadend = () => {
            this.setState({
                imageFile: file,
                image: reader.result
            })
        }

        reader.readAsDataURL(file)
    }
  }
  render() {
    return (
      <div className="row justify-content-center">
        <div className="row col-12 col-sm-8 justify-content-center">
          <img src={ this.state.image ? this.state.image : process.env.PUBLIC_URL + '/image/placeholder.png'} alt="Artwork" className="img-thumbnail avatar" style={{width : 200, height: 200}}  id="avatar" onClick={ this.onAvatar }/>
          <input ref={input => this.filePicker = input} type="file" name="pic" accept="image/*" hidden onChange={ this.onFilePick } />
        </div>
        <div className="row col-12 col-sm-8" style={{paddingTop: 20}}>
          <CustomTextField ref={(input) => { this.titleInput = input; }} id="vrtitle" hintText="Title" type="vrtitle" onhandleChange={this.onChange}/>
        </div>
        <div className="row col-12 col-sm-8" style={{paddingTop: 20}}>
          <CustomTextField ref={(input) => { this.descriptionInput = input; }} id="vrdescription" hintText="Description" type="vrdescription" onhandleChange={this.onChange}/>
          
        </div>
        <div className="row col-12 col-sm-8" style={{paddingTop: 20}}>
          <CustomTextField ref={(input) => { this.ratingInput = input; }} id="vrrating" hintText="Rating" type="vrrating" onhandleChange={this.onChange}/>
        </div>
        <div className="row col-12 col-sm-8 justify-content-end" style={{paddingTop: 20}}>
          <RaisedButton label="Submit" secondary={true} onClick={this.handleClick} style={{height: 48}} />
        </div>
      </div>
    );
  }
}

export default Vrcontent;
