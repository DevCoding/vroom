import React, { Component } from 'react';
import CustomTextField from '../../../components/customTextfield/customTextfield'
import RaisedButton from 'material-ui/RaisedButton';
import { saveMusic } from '../../../helpers/auth'
import ReactDOM from 'react-dom';
class Music extends Component {
    constructor(props){
        super(props);
        this.style = {
            container : {top : 20},
            audio : {"marginTop" : 20},
            
        }
        this.state = {
            artist : "",
            audiotitle : "",
            genre : "",
            description : "",
            image : null,
            imageFile : null,
            audioFile : null
        }
        this.onChange = this.onChange.bind(this);
        this.onAvatar = this.onAvatar.bind(this);
        this.onFilePick = this.onFilePick.bind(this);
        this.onAudioPick = this.onAudioPick.bind(this);
        this.handleClick = this.handleClick.bind(this)
    }
    onAvatar(e) {
        this.filePicker.click()
    }
    onAudioPick(e) {
        e.preventDefault()
        
        const file = e.target.files[0]

        if (file) {
            let reader = new FileReader()

            reader.onloadend = () => {
                this.setState({
                    audioFile: file,
                })
            }
            reader.readAsDataURL(file)
        }
    }
    onFilePick(e) {
        e.preventDefault()

        const file = e.target.files[0]

        if (file) {
            let reader = new FileReader()

            reader.onloadend = () => {
                this.setState({
                    imageFile: file,
                    image: reader.result
                })
            }

            reader.readAsDataURL(file)
        }
    }
    onChange(value, type){
        if (type == "artist"){
            var state = this.state
            state["artist"] = value.text
            this.setState(state)
        }
            
        if (type == "audiotitle"){
            var state = this.state
            state["audiotitle"] = value.text
            this.setState(state)
        }
        if (type == "genre"){
            var state = this.state
            state["genre"] = value.text
            this.setState(state)
        }
        if (type == "description"){
            var state = this.state
            state["description"] = value.text
            this.setState(state)
        }
    }
    handleClick = (e) => {
        console.log(this.state)

        if (this.state.artist && this.state.audiotitle && this.state.genre && this.state.description){
            saveMusic(this.state)


            this.setState({
                artist : "",
                audiotitle : "",
                genre : "",
                description : "",
                image : null,
                imageFile : null,
                audioFile : null,
            })

            this.artistInput.setState({text : ""})
            this.audiotitleInput.setState({text : ""})
            this.genreInput.setState({text : ""})
            this.descriptionInput.setState({text : ""})


            ReactDOM.findDOMNode(this.filePicker).value = "";
          } else{
            alert("Validation Failed")
          }
    }
    render() {
        return (
            <div>

                <div className="row d-flex justify-content-center " style={{paddingTop : 25}}>
                    <div className="row col-12 col-sm-8">
                        <div className="col-4 col-sm-4 d-flex justify-content-end" style={{color : 'white', fontSize : 20}}>
                            ArtWork
                        </div>
                        <div className="col-8 col-sm-8 d-flex justify-content-start" >
                            <img src={ this.state.image ? this.state.image : process.env.PUBLIC_URL + '/image/placeholder.png'} alt="Artwork" className="img-thumbnail avatar" style={{width : 200, height: 200}}  id="avatar" onClick={ this.onAvatar }/>
                            <input ref={input => this.filePicker = input} type="file" name="pic" accept="image/*" hidden onChange={ this.onFilePick } />
                        </div>
                    </div>
                    <div className="row col-12 col-sm-8" style={{paddingTop : 30}}>
                        <div className="col-4 col-sm-4 d-flex justify-content-end" style={{color : 'white', fontSize : 20}}>
                            Music&nbsp;&nbsp;
                        </div>
                        <div className="col-8 col-sm-8 d-flex justify-content-start" >
                            <input style={{width: '100%', color : 'white'}} ref={input => this.audioPicker = input} type="file" id="audiofile" accept="audio/*" onChange={ this.onAudioPick }/>
                        </div>
                    </div>
                    <div className="row col-12 col-sm-8" style={{paddingTop : 30}}>
                        <div className="col-4 col-sm-4 d-flex justify-content-end" style={{color : 'white', fontSize : 20}}>
                        Artist&nbsp;&nbsp;
                        </div>
                        <div className="col-8 col-sm-8 d-flex justify-content-start" >
                            <CustomTextField ref={(input) => { this.artistInput = input; }} id="artist" hintText="Artist" type="artist" onhandleChange={this.onChange}/>
                        </div>
                    </div>
                    <div className="row col-12 col-sm-8" style={{paddingTop : 25}}>
                        <div className="col-4 col-sm-4 d-flex justify-content-end" style={{color : 'white', fontSize : 20}}>
                        AudioTitle&nbsp;&nbsp;
                        </div>
                        <div className="col-8 col-sm-8 d-flex justify-content-start" >
                            <CustomTextField ref={(input) => { this.audiotitleInput = input; }} id="audiotitle" hintText="AudioTitle" type="audiotitle" onhandleChange={this.onChange}/>
                        </div>
                    </div>
                    <div className="row col-12 col-sm-8" style={{paddingTop : 25}}>
                        <div className="col-4 col-sm-4 d-flex justify-content-end" style={{color : 'white', fontSize : 20}}>
                        Genre&nbsp;&nbsp;
                        </div>
                        <div className="col-8 col-sm-8 d-flex justify-content-start" >
                            <CustomTextField ref={(input) => { this.genreInput = input; }} id="genre" hintText="Genre" type="genre" onhandleChange={this.onChange}/>
                        </div>
                    </div>
                    <div className="row col-12 col-sm-8" style={{paddingTop : 25}}>
                        <div className="col-4 col-sm-4 d-flex justify-content-end" style={{color : 'white', fontSize : 20}}>
                        Description&nbsp;&nbsp;
                        </div>
                        <div className="col-8 col-sm-8 d-flex justify-content-start" >
                            <CustomTextField ref={(input) => { this.descriptionInput = input; }} id="description" hintText="Description" type="description" onhandleChange={this.onChange}/>
                        </div>
                    </div>
                    <div className="row col-12 col-sm-8" style={{paddingTop : 25}}>
                        <div className="col-4 col-sm-4 d-flex justify-content-end" style={{color : 'white', fontSize : 20}}>
                        
                        </div>
                        <div className="col-8 col-sm-8 d-flex justify-content-end" >
                            <RaisedButton label="Submit" secondary={true} onClick={this.handleClick} style={{height: 48}}/>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default Music;
