import React, { Component } from 'react'

import { Switch, Route } from 'react-router-dom'
import CustomTextField from '../../components/customTextfield/customTextfield'

import asyncComponent from '../../components/asyncComponent/asyncComponent'
import { 
    Link
} from 'react-router-dom';
const AsyncMusic = asyncComponent(() => import('./music/music'))
const AsyncVrcontent = asyncComponent(() => import('./vrcontent/vrcontent'))
export default class Firetool extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return (
            <div className="container-fluid">
                <div className="row" style={{paddingBottom: 50}}>
                            <li className='first'>
                                <Link to="/firebase" style={{fontSize: 30, color : 'white'}}>VRContent</Link>
                            </li>
                            <li className='last' style={{marginLeft : 40}}>
                                <Link to="/firebase/music" style={{fontSize: 30, color : 'white'}}>Music</Link>
                            </li>
                    
                </div>
                <Switch path='/firebase'>
                    <Route exact path='/firebase' component={AsyncVrcontent}/>
                    <Route path='/firebase/music' component={AsyncMusic}/>
                </Switch>
            </div>
        );
    }

}
