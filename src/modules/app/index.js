import React, { Component } from 'react'
import { connect } from 'react-redux'
import routes from './routes'

import Login from '../login/login'
import Firetool from '../firetool/firetool'
import '../../assets/style.css'
class App extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return (
            <div className='container-fluid'>
                { routes(this.props) }
            </div>
        );
    }

}
const mapStateToProps = store => {
    return {
        auth: store.auth
      }
  }
  
  // Retrieve dispatch and callbacks from store as props
const mapDispatchToProps = (dispatch, props) => {
    return {
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(App)
