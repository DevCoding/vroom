import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import asyncComponent from '../../components/asyncComponent/asyncComponent'

const AsyncFirebase = asyncComponent(() => import('../firetool'))
const AsyncLogin = asyncComponent(() => import('../login'))

function PrivateRoute ({component: Component, authed, ...rest}) {
    return (
        <Route
        {...rest}
        render={(props) => authed == "LOGIN_SUCCEEDED"
            ? <Component {...props} />
            : <Redirect to={{pathname: '/login'}} />}
        />
    )
}
export default function routes(props){
    return (
        <Switch path='/'>
            <Route path='/login' component={AsyncLogin} /> 
            <Route exact path='/' render={() => <Redirect to="/login"/>}/> 
            <PrivateRoute authed={props.auth.state} path='/firebase' component={AsyncFirebase} /> 
            
        </Switch>
    );
}

  