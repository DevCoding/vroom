import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'

import CustomTextField from '../../../components/customTextfield/customTextfield'
import RaisedButton from 'material-ui/RaisedButton'
import '../../../assets/style.css'

import asyncComponent from '../../../components/asyncComponent/asyncComponent'
import UserSetting from './userSetting/userSetting'

const AsyncDriver = asyncComponent(() => import('./driver/driver'))
const AsyncPassenger = asyncComponent(() => import('./passenger/passenger'))
export default class RightLayout extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return (
            <div className="row justify-content-center">
                <div className="col-12 col-sm-12" style={{marginTop: 50}}>
                    <UserSetting />
                </div>
                <div className="col-12 col-sm-12" style={{marginTop: 50}}>
                    <Switch path='/login'>
                        <Route exact path='/login' component={AsyncPassenger}/>
                        <Route path='/login/driver' component={AsyncDriver}/>
                    </Switch>
                </div>
            </div>
        );
    }

}
