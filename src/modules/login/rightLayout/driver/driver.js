import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import CustomTextField from '../../../../components/customTextfield/customTextfield'
import RaisedButton from 'material-ui/RaisedButton'
import IconButton from 'material-ui/IconButton';
import '../../../../assets/style.css'

import { auth, saveUser } from '../../../../helpers/auth'
export default class Driver extends Component{
    
    constructor(props){
        super(props)

        this.state = {
            image : null,
            imageFile : null,

            fname : "",
            lname : "",
            title : "",
            majordept : "",
            email : "",
            phone : "",
            fquote : "",
            password : "",
            confirmpass : "",

            fname_valid : true,
            lname_valid : true,
            title_valid : true,
            majordept_valid : true,
            email_valid : true,
            phone_valid : true,
            fquote_valid : true,
            password_valid : true,
            confirm_valid : true,
        };

        this.onAvatar = this.onAvatar.bind(this)
        this.onFilePick = this.onFilePick.bind(this)
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }
    onSubmit(e) {
        if (this.state.password != this.state.confirmpass){
            alert("Password Validation Error")
            return;
        }
        if (this.state.fname && this.state.lname && this.state.title && this.state.majordept && this.state.email && this.state.phone && this.state.fquote && this.state.image != null && this.state.imageFile != null){
            if (this.state.fname_valid && this.state.lname_valid && this.state.title_valid && this.state.majordept_valid && this.state.email_valid && this.state.phone_valid && this.state.fquote_valid){
                let value = {
                    fname : this.state.fname, 
                    lname : this.state.lname, 
                    title : this.state.title, 
                    majordept : this.state.majordept,
                    email : this.state.email,
                    phone : this.state.phone,
                    fquote : this.state.fquote,
                    imageFile : this.state.imageFile
                }
                auth(this.state.email, this.state.password, value, "driver")
                .catch(e => alert('Signup Failed, reason is "' + e + '"'))
                .then(user => {
                    this.setState({
                        image : null,
                        imageFile : null,
            
                        fname_valid : true,
                        lname_valid : true,
                        title_valid : true,
                        majordept_valid : true,
                        email_valid : true,
                        phone_valid : true,
                        fquote_valid : true,
                        password_valid : true,
                        confirm_valid : true,
            
                        fname : "",
                        lname : "",
                        title : "",
                        majordept : "",
                        email : "",
                        phone : "",
                        fquote : "",
                        password : "",
                        confirmpass : "",
                    });
                    ReactDOM.findDOMNode(this.filePicker).value = "";

                    this.fnameInput.setState({text : ""})
                    this.lnameInput.setState({text : ""})
                    this.titleInput.setState({text : ""})
                    this.majordeptInput.setState({text : ""})
                    this.emailInput.setState({text : ""})
                    this.phoneInput.setState({text : ""})
                    this.fquoteInput.setState({text : ""})
                    this.passwordInput.setState({text : ""})
                    this.confirmpasswordInput.setState({text : ""})
                    saveUser(value,"driver")
                })
            } else {
                alert("Validation Error")
            }
        } else {
            alert("Validation Error")
        }
    }
    onAvatar(e) {
        this.filePicker.click()

    }
    onFilePick(e) {
        e.preventDefault()

        const file = e.target.files[0]

        if (file) {
            let reader = new FileReader()

            reader.onloadend = () => {
                this.setState({
                    imageFile: file,
                    image: reader.result
                })
            }

            reader.readAsDataURL(file)
        }
    }
    onChange(value, type){
        if (type == "fname"){
            var state = this.state
            state["fname"] = value.text
            state["fname_valid"] = value.valid
            this.setState(state)
        }
            
        if (type == "lname"){
            var state = this.state
            state["lname"] = value.text
            state["lname_valid"] = value.valid
            this.setState(state)
        }
        if (type == "title"){
            var state = this.state
            state["title"] = value.text
            state["title_valid"] = value.valid
            this.setState(state)
        }
        if (type == "majordept"){
            var state = this.state
            state["majordept"] = value.text
            state["majordept_valid"] = value.valid
            this.setState(state)
        }
        if (type == "email"){
            var state = this.state
            state["email"] = value.text
            state["email_valid"] = value.valid
            this.setState(state)
        }
        if (type == "phone"){
            var state = this.state
            state["phone"] = value.text
            state["phone_valid"] = value.valid
            this.setState(state)
        }
        if (type == "fquote"){
            var state = this.state
            state["fquote"] = value.text
            state["fquote_valid"] = value.valid
            this.setState(state)
        }
        if (type == "password"){
            var state = this.state
            state["password"] = value.text
            state["password_valid"] = value.valid
            this.setState(state)
        }
        if (type == "confirm"){
            var state = this.state
            state["confirmpass"] = value.text
            state["confirm_valid"] = value.valid
            this.setState(state)
        }
    }
    render(){
        return (
            <div className="row justify-content-center">
                <div className='row justify-content-center align-items-center col-12 col-sm-12 p-1'>
                    <img src={ this.state.image ? this.state.image : process.env.PUBLIC_URL + '/image/placeholder-man.png' } alt="avatar" className="img-thumbnail avatar" style={{width : 106, height: 106}}  id="avatar" onClick={ this.onAvatar }/>
                    <input ref={input => this.filePicker = input} type="file" name="pic" accept="image/*" hidden onChange={ this.onFilePick } />
                </div>
                <div className='row justify-content-center align-items-center col-12 col-sm-12 p-1'>
                    <div className="col col-sm-6 p-1">
                        <CustomTextField ref={(input) => { this.fnameInput = input; }} id="fname" hintText="First Name" type="fname"  onhandleChange={this.onChange}/>
                    </div>
                    <div className="col col-sm-6 p-1">
                        <CustomTextField ref={(input) => { this.lnameInput = input; }} id="lname" hintText="Last Name" type="lname" onhandleChange={this.onChange}/>
                    </div>
                </div>
                <div className='row justify-content-center align-items-center col-12 col-sm-12 p-1'>
                    <div className="col col-sm-3 p-1">
                        <CustomTextField ref={(input) => { this.titleInput = input; }} id="title" hintText="Title" type="title" onhandleChange={this.onChange}/>
                    </div>
                    <div className="col col-sm-9 p-1">
                        <CustomTextField ref={(input) => { this.majordeptInput = input; }} id="majordept" hintText="Major or Dept" type="majordept" onhandleChange={this.onChange}/>
                    </div>
                </div>
                <div className='row justify-content-center align-items-center col-12 col-sm-12 p-1'>
                    <div className="col col-sm-6 p-1">
                        <CustomTextField ref={(input) => { this.emailInput = input; }} id="email" hintText="Email" type="email" onhandleChange={this.onChange}/>
                    </div>
                    <div className="col col-sm-6 p-1">
                        <CustomTextField ref={(input) => { this.phoneInput = input; }} id="phone" hintText="Phone" type="phone" onhandleChange={this.onChange}/>
                    </div>
                </div>
                <div className='row justify-content-center align-items-center col-12 col-sm-12 p-1'>
                    <div className="col col-sm-12 p-1">
                        <CustomTextField ref={(input) => { this.fquoteInput = input; }} id="fquote" hintText="Enter a quote here.." type="fquote" onhandleChange={this.onChange}/>
                    </div>
                </div>
                <div className='row justify-content-center align-items-center col-12 col-sm-12 p-1'>
                    <div className="col col-sm-12 p-1">
                        <CustomTextField ref={(input) => { this.passwordInput = input; }} id="password" hintText="Create Password" type="password" onhandleChange={this.onChange}/>
                    </div>
                </div>
                <div className='row justify-content-center align-items-center col-12 col-sm-12 p-1'>
                    <div className="col col-sm-12 p-1">
                        <CustomTextField ref={(input) => { this.confirmpasswordInput = input; }} id="confirmpassword" hintText="Enter Password Again" type="confirm" onhandleChange={this.onChange}/>
                    </div>
                </div>
                <div className='row col-12 col-sm-12 p-2'>
                    <RaisedButton label="Submit" secondary={true} onClick={this.onSubmit} style={{width: '100%',height: 48}}/>
                </div>
            </div>
            
        );
    }

}
