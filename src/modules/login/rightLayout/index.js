import RightLayout from './rightLayout';

import React from 'react'

import { Redirect, Switch, Route } from 'react-router-dom'

import { connect } from 'react-redux'
const mapStateToProps = store => {
    return {
        auth: store.auth
    }
  }
  
  // Retrieve dispatch and callbacks from store as props
const mapDispatchToProps = dispatch => {
    return {
        
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RightLayout)