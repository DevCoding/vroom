import React, { Component } from 'react'

import { Route, BrowserRouter, Link, Redirect, Switch, NavLink } from 'react-router-dom'

import CustomTextField from '../../../../components/customTextfield/customTextfield'
import RaisedButton from 'material-ui/RaisedButton'
import IconButton from 'material-ui/IconButton';
import '../../../../assets/style.css'
export default class UserSetting extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return (
            <div className="row justify-content-center">
                <div className="row justify-content-center align-items-center col-12 col-sm-12">
                    <img src={process.env.PUBLIC_URL + '/image/redcar.png'} alt="" style={{width : 50, height : 50}}/>
                    <div style={{ color : 'black', fontSize :30, background : '#FFC107', marginLeft : 10}}>VROOM TO DRIVE</div>  
                </div>
                <div className="row justify-content-center align-items-center col-12 col-sm-12 ">
                    <div style={{ color : 'white', fontSize :30,  marginLeft : 15, paddingTop : 0}}>SIGN UP</div>  
                    <IconButton>
                        <a href="/login/driver">
                            <img src={process.env.PUBLIC_URL + '/image/triangle.png'} alt=""/>
                        </a>
                    </IconButton>
                </div>
                <div className='row justify-content-center align-items-center col-12 col-sm-12'>
                    <img src={process.env.PUBLIC_URL + '/image/line.png'} alt="" style={{width: '70%'}}/>
                </div>

                <div className="row justify-content-center align-items-center col-12 col-sm-12" style={{paddingTop: 50}}>
                    <img src={process.env.PUBLIC_URL + '/image/redcar.png'} alt="" style={{width : 50, height : 50}}/>
                    <div style={{ color : 'black', fontSize :30, background : '#FFC107', marginLeft : 10}}>VROOM TO RIDE&nbsp;</div>  
                </div>
                <div className="row justify-content-center align-items-center col-12 col-sm-12 ">
                    <div style={{ color : 'white', fontSize :30,  marginLeft : 15, paddingTop : 0}}>SIGN UP</div>  
                    <IconButton>
                        <a href="/login">
                            <img src={process.env.PUBLIC_URL + '/image/triangle.png'} alt=""/>
                        </a>
                    </IconButton>
                </div>
                <div className='row justify-content-center align-items-center col-12 col-sm-12'>
                    <img src={process.env.PUBLIC_URL + '/image/line.png'} alt="" style={{width: '70%'}}/>
                </div>
            </div>
            
        );
    }

}
