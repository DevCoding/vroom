import React, { Component } from 'react'


import CustomTextField from '../../../components/customTextfield/customTextfield'
import RaisedButton from 'material-ui/RaisedButton'
import IconButton from 'material-ui/IconButton'
import '../../../assets/style.css'
export default class LeftLayout extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return (
            <div className="row justify-content-center">
                <div className="col-12 col-sm-12" style={{marginTop: 50, textAlign: 'center'}}>
                    <img src={process.env.PUBLIC_URL + '/image/vrroom.png'} style={{width: '80%'}}/>
                </div>
                <div className="col-12 col-sm-12" style={{marginTop : 100, color : 'white', fontSize : 35, textAlign: 'center'}}>
                    <p>
                        Its the Journey not the Destination
                    </p>
                    <br/>
                    <br/>
                    <p>
                        Real Rides. Unreal Experiences
                    </p>
                    <br/>
                    <br/>
                    <p style={{color : '#59BEBA'}}>
                        Please download the Android or IOS App to order the VROOM Service
                    </p>
                    
                </div>
                <div className="row col-12 col-sm-12 justify-content-around" style={{marginTop : 50}}>
                    <div className="col-12 col-sm-4 p-1" style={{textAlign: 'center'}}>
                        <RaisedButton style={{width: 150, height: 60}}>
                            <img src={process.env.PUBLIC_URL + '/image/appstore.png'} style={{width: 150, height: 60}}/>
                        </RaisedButton>
                    </div>
                    <div className="col-12 col-sm-4 p-1" style={{textAlign: 'center'}}>
                        <RaisedButton style={{width: 150, height: 60}}>
                            <img src={process.env.PUBLIC_URL + '/image/googleplay.png'} style={{width: 150, height: 60}}/>
                        </RaisedButton>
                    </div>            
                    {/* <img src={process.env.PUBLIC_URL + '/image/googleplay.png'} className="col-12 col-sm-4"/> */}
                </div>
            </div>
        );
    }

}
