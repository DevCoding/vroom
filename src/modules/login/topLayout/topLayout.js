import React, { Component } from 'react'
import ReactDOM from 'react-dom';

import { connect } from 'react-redux'
import CustomTextField from '../../../components/customTextfield/customTextfield'
import RaisedButton from 'material-ui/RaisedButton'
import '../../../assets/style.css'

import { login } from '../../../helpers/auth'
import { loginRequested } from '../../../redux/action/auth'
export default class TopLayout extends Component{
    constructor(props){
        super(props)

        this.state = {
            email : "",
            password : "",
        }
        this.onChange = this.onChange.bind(this)
        this.onLogin = this.onLogin.bind(this)
    }
    onHelp = (e) => {
        e.preventDefault();
        
        alert("Help")
    }
    onLogin = (e) => {
        e.preventDefault();



        if (this.state.email == "admin@gmail.com" && this.state.password == "password"){
            if (this.props.auth.state === 'LOGGING_IN')
                return
            this.props.loginRequested(this.state.email, this.state.password)
            login(this.state.email, this.state.password)
            .then((user) => {
                alert("Successfully Login");
                this.props.loginSucceeded(this.state)

                this.props.history.push('/firebase')
            })
            .catch((error) => {
                alert(error);
                this.props.loginFailed(error)
            })
        }
        else {
            alert("You are not the admin")
        }
    }
    onChange = (value, type) => {
        if (type === "email")
            this.setState({email : value.text})
        if (type === "password")
            this.setState({password : value.text})
    }
    render(){
        return (
            <header>
                <div className="row">
                    <div className="col-12 col-sm-4 p-2">
                        <CustomTextField id="email" hintText="Email" type="email" onhandleChange={this.onChange} ref={(input) => { this.emailText = input; }}/>
                    </div>
                    <div className="col-12 col-sm-4 p-2">
                        <CustomTextField id="password" hintText="Password" type="password" onhandleChange={this.onChange}/>
                    </div>
                    <div className="col-12 col-sm-2 p-2">
                        <RaisedButton label="Login" secondary={true}  onClick={this.onLogin} style={{height: 48, width: '100%'}}/>
                    </div>
                    <div className="col-12 col-sm-2 p-2">
                        <RaisedButton label="Help" primary={true} onClick={this.onHelp} style={{height: 48, width: '100%'}}/>
                    </div>
                </div>
            </header>
        );
    }

}