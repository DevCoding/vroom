import TopLayout from './topLayout';

import React from 'react'
import { connect } from 'react-redux'
import { Redirect, Switch, Route, withRouter } from 'react-router-dom'
import { loginRequested, loginSucceeded, loginFailed } from '../../../redux/action/auth'
const mapStateToProps = store => {
    return {
        auth: store.auth
      }
  }
  
  // Retrieve dispatch and callbacks from store as props
const mapDispatchToProps = (dispatch, props) => {
    return {
        loginRequested : (email, password) => dispatch(loginRequested(email, password)),
        loginSucceeded : (state) => dispatch(loginSucceeded(state)),
        loginFailed : (err) => dispatch(loginFailed(err)),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TopLayout))