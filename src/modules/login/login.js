import React, { Component } from 'react'


import CustomTextField from '../../components/customTextfield/customTextfield'
import '../../assets/style.css'

import TopLayout from './topLayout'
import LeftLayout from './leftLayout'
import RightLayout from './rightLayout'
export default class Login extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return (
            <div className="container-fluid">
                <TopLayout />
                <div className="row">
                    <div className="col-12 col-sm-8">
                        <LeftLayout />
                    </div>
                    <div className="col-12 col-sm-4">
                        <RightLayout />
                    </div>
                </div>
                
            </div>
            
        );
    }

}
