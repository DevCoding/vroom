import React, { Component } from 'react';
import TextField from 'material-ui/TextField'
import './style.css'
class CustomTextField extends Component {
    constructor(props){
        super(props)


        this.state = {
            text : "",
            valid : true,
        }
        this.style = {
            config_valid : {
                border: '0.5px solid #FF9800',
                paddingLeft : 10,
                width : '100%'
            },
            config_invalid : {
                border: '0.5px solid #F44336',
                paddingLeft : 10,
                width : '100%'
            },
        }
        this.changeValue = this.changeValue.bind(this)
    }

    changeValue(e, type) {
        var validateEmail = (email)=>{
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
        var validateName = (name)=>{
            if (name){
                return true
            } else return false
        }
        var validatePhone = (phone)=>{
            var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/; 
            return phoneno.test(phone);
        }
        var valid = true;

        if (type == "vrtitle" || type == "vrdescription" || type == "vrrating"){
            valid = true
        }
        if (type == "artist" || type == "audiotitle" || type == "genre" || type == "description"){
            valid = true
        }

        if (type == "fname"){
            valid = validateName(e.target.value)
        }

        if (type == "lname"){
            valid = validateName(e.target.value)
        }
        if (type == "title"){
            valid = validateName(e.target.value)
        }
        if (type == "majordept"){
            valid = validateName(e.target.value)
        }
        if (type == "email"){
            valid = validateEmail(e.target.value)
        }
        if (type == "phone"){
            valid = validatePhone(e.target.value)
        }
        if (type == "fquote"){
            // valid = e.target.value.length < 1 ? false : true
            valid = validateName(e.target.value)
        }
        
        if (type == "password"){
            valid = e.target.value.length < 1 ? false : true
        }
        if (type == "confirm"){
            valid = e.target.value.length < 1 ? false : true
        }
        var state = this.state
        state["valid"] = valid
        state["text"] = e.target.value

        this.setState(state)

        this.props.onhandleChange(this.state, type);
    }
    render() {
        return (
            <TextField
                ref = {this.props.ref}
                id={this.props.id}
                className={this.props.className} 
                hintText={this.props.hintText} 
                underlineShow={false}  
                style={ this.state.valid == true ? this.style.config_valid : this.style.config_invalid} 
                hintStyle={{color : 'white'}}
                inputStyle={{color : 'white'}}
                onChange={e => this.changeValue(e, this.props.type)}
                errorStyle = {{color : 'white', paddingTop : 10}}
                value = {this.state.text}
                type={this.props.type == "password" || this.props.type == "confirm" ? "password" : "text"}
            />
        )
    }
}
export default CustomTextField;