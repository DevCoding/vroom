import React, { Component } from 'react';
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'
import PropTypes from 'prop-types'
import { 
  BrowserRouter as Router,
  Route, Switch
} from 'react-router-dom';
// import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Core from './modules/app';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

export default function App(props) {
  return (
      <Provider store={props.store}>
        <MuiThemeProvider>
          <ConnectedRouter history={props.history}>
            <Route component={Core} />
          </ConnectedRouter>
        </MuiThemeProvider>
      </Provider>
  )
}
App.propTypes = {
  store : PropTypes.object.isRequired
}
